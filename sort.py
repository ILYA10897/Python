# сортировка выбором Решетников И.С. УИБ-14-1
def Sort_vibor(arrayToSort):
    v = arrayToSort
    for i in range(len(v)):
        idxMin = i
        for j in range(i+1, len(v)):
            if v[j] < v[idxMin]:
                idxMin = j
        tmp = v[idxMin]
        v[idxMin] = v[i]
        v[i] = tmp
    return v
ary = [0,3,5,1,2,3,5,4,2,34,43,24]
print Sort_vibor(ary)
# cортировка вставками. Решетников И.С. УИБ-14-1
def Vstavka_sort(arrayToSort):
    a = arrayToSort
    for i in range(len(a)):
        v = a[i]
        j = i;
        while (a[j-1] > v) and (j > 0):
            a[j] = a[j-1]
            j = j - 1
        a[j] = v
        print a
    return a

ary = [54,1,2,3,52,3,1,2,3,5,3,67,3,2,543]
print Vstavka_sort(ary)
#сортировка пузырьком Решетников И.С. УИБ-14-1
def bubble_sort(arrayToSort):
    a = arrayToSort
    for i in range(len(a),0,-1):
        for j in range(1, i):
            if a[j-1] > a[j]:
                tmp = a[j-1]
                a[j-1] = a[j]
                a[j] = tmp
                print a
    return a
ary = [5, 0, 10, 4, 1, 5, 8, 4, 3, 12, 41]
print bubble_sort(ary)
